﻿using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash_Assessment_
{
    class Wall:Tile
    {
         // ------------------
        // Behaviour
        // ------------------
        public Wall(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
