﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash_Assessment_
{
    class Sprite
    {
        // -------------------
        // Data
        // -------------------
        protected Vector2 position;
        protected Texture2D texture;
        protected bool visible = true;


        // ------------------
        // Behaviour
        // ------------------
        public Sprite(Texture2D newTexture)
        {
            texture = newTexture;
        }
        // ------------------
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(texture, position, Color.White);
            }
        }
        // ------------------
        public bool GetVisible()
        {
            return visible;
        }
        // ------------------
        public void SetVisible(bool newVisible)
        {
            visible = newVisible;
        }
        // ------------------

        // ------------------
        public void SetPosition(Vector2 newPostion)
        {
            position = newPostion;
        }
        // ------------------
        public virtual Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
        }
        // ------------------
        public void DrawBounds(SpriteBatch spriteBatch, GraphicsDevice graphics)
        {
            //uses our bounds to calcualte texture size
            Rectangle bounds = GetBounds();
            // creates empty texture to be drawn
            Texture2D boundsTexture = new Texture2D(graphics, bounds.Width, bounds.Height);
            // fill in the texture with white
            Color[] colorData = new Color[bounds.Width * bounds.Height];
            for (int i = 0; i < colorData.Length; ++i)
            {
                colorData[i] = Color.White;
            }
            boundsTexture.SetData(colorData);

            spriteBatch.Draw(boundsTexture, position, Color.White);
        }
        // ------------------
    }
}

