﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoulderDash_Assessment_
{
    class Boulder:Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        
        

        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 1.5f;

        //consants


        // ------------------
        // Behaviour
        // ------------------
        public Boulder(Texture2D newTexture, Level newLevel)
            : base(newTexture)
        {
            ourLevel = newLevel;
        }
        // ------------------
        public bool TryPush(Vector2 Direction)
        {
            // New position the boulder will be in after the push
            Vector2 newGridPos = GetTilePosition() + Direction;
            


            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);
            

            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Boulder)
            {
                return false;
            }

            // If the target tile dirt, we can't move the boulder there so return false
            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }

            // If the target tile dirt, we can't move the boulder there so return false
            

            if (tileInDirection != null && tileInDirection is Diamond)
            {
                return false;
            }


            

            // Move our tile (boulder) to the new position
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public bool Gravity()
        {
            Vector2 direction = new Vector2();
            direction.Y = 1f;
            Vector2 newGridPos = GetTilePosition() + direction; 
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Player)
            {
                ourLevel.ResetLevel();
            }
            if (tileInDirection != null && tileInDirection is Boulder)
            {


                direction.X = 1f;
                newGridPos = GetTilePosition() + direction;
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = -1f;
                    newGridPos = GetTilePosition() + direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                    if ((tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                     || tileInDirection is Boulder || tileInDirection is Diamond))
                    {
                        direction.X = 0f;
                        direction.Y = 1f;

                        return false;
                    }
                }





            }
            if (tileInDirection != null && tileInDirection is Diamond)
            {


                direction.X = 1f;
                newGridPos = GetTilePosition() + direction;
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = -1f;
                    newGridPos = GetTilePosition() + direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                    if ((tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                     || tileInDirection is Boulder || tileInDirection is Diamond))
                    {
                        direction.X = 0f;
                        direction.Y = 1f;

                        return false;
                    }
                }




            }
            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }


            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Add to time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            // If there is nothing under the boulder let it fall on a timer
            if (timeSinceLastMove >= MOVE_COOLDOWN)
            {
                Gravity();
                timeSinceLastMove = 0;
            }


        }

    }
}
