﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoulderDash_Assessment_
{
    class Diamond : Tile
    {

        private int scoreValue = 1;

        private Level ourLevel;



        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 1.5f;

        public Diamond(Texture2D newTexture, Level newlevel)
                : base(newTexture)
        {

            ourLevel = newlevel;
        }


        public bool Gravity()
        {
            Vector2 direction = new Vector2();
            direction.Y = -1f;
            Vector2 newGridPos = GetTilePosition() - direction;
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Boulder)
            {

                direction.Y = 0f;
                direction.X = -1f;
                newGridPos = GetTilePosition() - direction;
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = 1f;
                    newGridPos = GetTilePosition() - direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);
                }
                else if ((tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond))
                {
                    direction.X = 0f;
                    direction.Y = -1f;

                    return false;
                }


            }

            if (tileInDirection != null && tileInDirection is Diamond)
            {

                direction.Y = 0f;
                direction.X = -1f;
                newGridPos = GetTilePosition() - direction;
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = 1f;
                    newGridPos = GetTilePosition() - direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);
                }
                else if ((tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond))
                {
                    direction.X = 0f;
                    direction.Y = -1f;

                    return false;
                }


            }

            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }

            if (tileInDirection != null && tileInDirection is Player)
            {
                ourLevel.ResetLevel();
            }


            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);
            return ourLevel.TryMoveTile(this, newGridPos);
        }

        public override void Update(GameTime gameTime)
        {
            // Add to time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            // If there is nothing under the boulder let it fall on a timer
            if (timeSinceLastMove >= MOVE_COOLDOWN)
            {
                Gravity();
                timeSinceLastMove = 0;
            }


        }


        public int GetScore()
        {
            return scoreValue;
        }


    }
}

