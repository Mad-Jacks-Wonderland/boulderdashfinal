﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash_Assessment_
{
    class Goal : Tile
    {

        private Level ourLevel;
        private Player player;

        private Texture2D closedDoorTexture;
        private Texture2D openDoorTexture;

        public Goal(Texture2D newClosedDoorTexture, Texture2D newOpenDoorTexture, Level newLevel)
            : base(newClosedDoorTexture)
        {
            ourLevel = newLevel;
            closedDoorTexture = newClosedDoorTexture;
            openDoorTexture = newOpenDoorTexture;
        }

        public void OpenDoor()
        {
            if (ourLevel.GetScore() == 5)
            {
                texture = openDoorTexture;
            }
            else
            {
                texture = closedDoorTexture;
            }
        }
    }
}
