﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BoulderDash_Assessment_
{
    class Player : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private int score = 0;
        private float timeSinceLastMoved = 0;
        private bool onGoal = false;


        // ------------------
        // Constants
        // ------------------
        private const float MOVE_COOLDOWN = 0.4f;

        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture, Level newlevel)
                : base(newTexture)
        {

            ourLevel = newlevel;
        }

        public override void Update(GameTime gameTime)
        {
            //add to time
            float frametime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMoved += frametime;

            KeyboardState keyboardState = Keyboard.GetState();

            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
            }

            if (movementInput != Vector2.Zero && timeSinceLastMoved >= MOVE_COOLDOWN)
            {
                TryMove(movementInput, gameTime);
                timeSinceLastMoved = 0;
            }



        }

        private bool TryMove(Vector2 direction, GameTime gameTime)
        {
            Vector2 newGridpos = GetTilePosition() + direction;

            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridpos);

            // if target tile if wall, return false
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }

            // if target tile is boulder, push it
            if (tileInDirection != null && tileInDirection is Boulder)
            {
                Boulder targetBoulder = tileInDirection as Boulder;
                bool pushSucceed = targetBoulder.TryPush(direction);

                if (pushSucceed == false)
                {
                    return false;
                }
            }

            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridpos);
            if (floorInDirection != null && floorInDirection is Goal)
            {
                EnterGoal();
            }



            if (tileInDirection != null && tileInDirection is Diamond)
            {
                HandleCollision(ourLevel.GetDiamond());
            }

            // need to ask level to move to new position
            bool moveResult = ourLevel.TryMoveTile(this, newGridpos);

            return moveResult;
        }

        public void HandleCollision(Diamond hitDiamond)
        {

            // collect diamond                    

            // add to score
            score += hitDiamond.GetScore();
        }

        public int GetScore()
        {
            return score;
        }

        private void EnterGoal()
        {
            onGoal = true;

            ourLevel.EvaluateVictory();
        }

        public bool GetOnGoal()
        {
            return onGoal;
        }

    }
}
