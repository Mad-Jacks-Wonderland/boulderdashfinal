﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoulderDash_Assessment_
{
    class Text
    {

        // Enums
        public enum Alignment
        {
            TOP_LEFT,
            TOP,
            TOP_RIGHT,
            LEFT,
            CENTRE,
            RIGHT,
            BOTTUM_LEFT,
            BOTTUM,
            BOTTUM_RIGHT
        }



        // Data
        SpriteFont font;
        string textString = "";
        Vector2 position;
        Color color = Color.White;
        Alignment alignment;


        public Text(SpriteFont newFont)
        {
            font = newFont;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 adjustedPosition = position;
            Vector2 textSize = font.MeasureString(textString);
            switch (alignment)
            {
                case Alignment.TOP_LEFT:
                    break;
                // default alignment
                case Alignment.TOP:
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.TOP_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    break;
                case Alignment.LEFT:
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case Alignment.CENTRE:
                    adjustedPosition.Y -= textSize.Y / 2;
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.RIGHT:
                    adjustedPosition.X -= textSize.X;
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case Alignment.BOTTUM_LEFT:
                    adjustedPosition.Y -= textSize.Y;
                    break;
                case Alignment.BOTTUM:
                    adjustedPosition.Y -= textSize.Y;
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case Alignment.BOTTUM_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    adjustedPosition.Y -= textSize.Y;
                    break;
            }

            
            spriteBatch.DrawString(font, textString, adjustedPosition, color);
            

        }

        public void SetTextString(string newText)
        {
            textString = newText;
        }

        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }

        public void SetColor(Color newColor)
        {
            color = newColor;
        }

        public void SetAlignment(Alignment newAlignment)
        {
            alignment = newAlignment;
        }

    }
}

