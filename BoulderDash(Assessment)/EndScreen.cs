﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;


namespace BoulderDash_Assessment_
{
    class EndScreen : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Text gameName;
        private Text startPrompt;
        private Game1 game;
        private Level ourLevel;

        // ------------------
        // Behaviour
        // ------------------
        public EndScreen(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont smallFont = content.Load<SpriteFont>("fonts/mainFont");

            gameName = new Text(titleFont);
            gameName.SetTextString("You finished the game!");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 300));

            startPrompt = new Text(smallFont);
            startPrompt.SetTextString("[Press R to play again!]");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.White);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200));
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.Draw(spriteBatch);
            startPrompt.Draw(spriteBatch);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            //Check if the player has pressed enter

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // If the player has pressed enter.....

            if (keyboardState.IsKeyDown(Keys.R))
            {
                //TODO: CHANGE TO LEVEL SCREEN

                game.ChangeScreen("level");

            }
        }
        // -------------------
    }

}

