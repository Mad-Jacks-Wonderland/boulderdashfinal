﻿using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash_Assessment_
{
    class Dirt: Tile
    {
        // ------------------
        // Behaviour
        // ------------------
        public Dirt(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
